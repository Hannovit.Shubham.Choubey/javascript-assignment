//1. Write a JavaScript function that checks whether a passed string is a palindrome or not?A palindrome is word, phrase, or sequence that reads the same backward as forward, e.g., madam or nurses run.
function palindrome(str){
    var reverse='';
    for(let i=str.length-1;i>=0;i--){
        reverse=reverse+str[i];
    }
    return str===reverse;
}
console.log(palindrome('madam'));



//2. Write a JavaScript program to pass a 'JavaScript function' as a parameter.
// passing js function palindrome as a parameter
function sendfunction(func){
    console.log("inside send function");
    console.log("calling palindrome function('madam')");
    console.log(func('madam'));
    console.log('exiting send fucntion');
}
sendfunction(palindrome);



//3. Write a JavaScript program to sum 3 and 5 multiples under 1000.
function sum3and5(n){
    let sum=0;
    for(let i =3;i<n;i++){
        if(i%3==0 || i%5==0){
            sum=sum+i;
        }
    }
    return sum;
}
console.log(sum3and5(1000));



// 4.Write a JavaScript program that implements a "form" validation that displays an error message if a required field is left empty when submitting the form.
function validation(event){
    event.preventDefault();
    let name = document.getElementById('name');
    let email = document.getElementById('email');
    let password = document.getElementById('password');
    const errorContainer = document.getElementById('errorContainer');
    const errorElement = document.createElement('p');
    if(!name.value || !email.value || !password.value){
        errorElement.textContent = 'fields cant be empty';
    }
    errorElement.textContent = 'success';
    errorContainer.appendChild(errorElement);
}



// 5. Write a JavaScript program to implement drag-and-drop functionality to allow users to reorder items in a list.
function dragDrop(){
    let left=document.getElementById("left");
    let right=document.getElementById("right");
    let lists=document.getElementsByClassName("list");

    for(list of lists){
        list.addEventListener("dragstart", function(e){
            let selected = e.target;
            right.addEventListener("dragover",function(e){
                e.preventDefault();
            });
            right.addEventListener("drop",function(e){
                right.appendChild(selected);
                selected=null;
            });
            left.addEventListener("dragover",function(e){
                e.preventDefault();
            });
            left.addEventListener("drop",function(e){
                left.appendChild(selected);
                selected=null;
            });
        })
    }
}
dragDrop();



// 6. Write a JavaScript program that adds a keydown event listener to a text input to detect when the "Enter key" is pressed.
function question6(){
    let textinput = document.getElementById("text");
    textinput.addEventListener("keydown",function(event){
        if(event.key==='Enter')
        alert('enter key pressed');
    })
}
question6();



// 7. Write a JavaScript function that takes a callback and invokes it after a delay of 2 second.
function question7(callbackfunction) {
    console.log('entered function')
    setTimeout(function() {
        callbackfunction();
    }, 2000); 
    console.log('exiting function')
}
function normalfunction() {
    console.log("Callback invoked after 2 seconds!");
}
question7(normalfunction);



// 8. Write a JavaScript program to create a class called "Person" with properties for name, age and country. Include a method to display the person's details. Create two instances of the 'Person' class and display their details.
class Person{
    constructor(name,age,country){
        this.name=name;
        this.age=age;
        this.country=country;
    }

    displayAllDetails(){
        console.log(`Name: ${this.name}, Age: ${this.age}, Country: ${this.country}`);
    }
}

let instance1 = new Person("Shubham",22,"India");
let instance2 = new Person("sam",17,"US");
 instance1.displayAllDetails();
 instance2.displayAllDetails();




// 9. Write a JavaScript program to create a class called 'Rectangle' with properties for width and height. Include two methods to calculate rectangle area and perimeter. Create an instance of the 'Rectangle' class and calculate its area and perimeter.
class Rectangle{
    area(width,height){
        return height* width;
    }
    perimeter(width,height){
        return 2* (height+width);
    }
}
 let instance = new Rectangle();
 console.log(instance.area(10,10));
 console.log(instance.area(5,5));
 console.log(instance.perimeter(10,10));




// 10. Write a JavaScript program that creates a class called 'Employee' with properties for name and salary. Include a method to calculate annual salary. Create a subclass called 'Manager' that inherits from the 'Employee' class and adds an additional property for department. Override the annual salary calculation method to include bonuses for managers. Create two instances of the 'Manager' class and calculate their annual salary.
class Employee{
    constructor(name,salary){
        this.name=name;
        this.salary=salary;
    }
    calculateAnnualSalary(){
        return this.salary*12;
    }
}
class Manager extends Employee {
    constructor(name,salary,department){
        super(name,salary);
        this.department= department;
    }
    calculateAnnualSalary(){
        const basic_salary= super.calculateAnnualSalary();
        const bonus=0.2*basic_salary;
        return basic_salary+ bonus;
    }
}
 let manager1= new Manager("Shubham",50000,'Manager');
 let manager2= new Manager("Bob",60000,'Manager');
 console.log(manager1.calculateAnnualSalary());
 console.log(manager2.calculateAnnualSalary());




// 11. Write a JavaScript program that creates a class called Product with properties for product ID, name, and price. Include a method to calculate the total price by multiplying the price by the quantity. Create a subclass called PersonalCareProduct that inherits from the Product class and adds an additional property for the warranty period. Override the total price calculation method to include the warranty period. Create an instance of the PersonalCareProduct class and calculate its total price.
class Product {
    constructor(productID, name, price, quantity) {
        this.productID = productID;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }
    totalPrice() {
        return this.price * this.quantity;
    }
}

class PersonalcareProduct extends Product {
    constructor(productID, name, price, quantity, warrantyPeriod) {
        super(productID, name, price, quantity);
        this.warrantyPeriod = warrantyPeriod;
    }

    totalPrice() {
        const basePrice = super.totalPrice();
        const warrantyPrice = this.warrantyPeriod * 5 * this.quantity;// assuming rs 5 perwarranty period
        return basePrice + warrantyPrice;
    }
}
const shampoo = new PersonalcareProduct("P1", "head & shoulders", 10, 20, 1);
 console.log(`Total Price: ${shampoo.totalPrice()}`);

// 12. Write a JavaScript function to generate a random integer.
// let randomnumber = (min,max)=>{
//     return Math.floor(Math.random() * (max - min) + min);
// }
// console.log(randomnumber(1,10));




// 13.Write a JavaScript function to get the greatest common divisor (GCD) of two integers.
function gcd(first,second){
    let min = (first >= second)?second:first;
    while(min>1){
        if(first%min ==0 && second % min == 0)
        return min;
        min--;
    }
    return 1;
}
 console.log(gcd(8,12));




//14. Write a JavaScript function to check whether an `input` is an array or not.
function checkArray(input)
{
    let length= input.length;
    if(length>1){
        console.log("It is array");
    }else
    console.log("It is a single input");
}
 checkArray(1);
 checkArray([1,2,3,4]);




// 15. Write a JavaScript program to compute the sum and product of an array of integers.
function sumProduct(){
    let inputArray= document.getElementById("15").value;
    const numbers = inputArray.split(',').map(Number);
    let sum=0;
    let product =1;
    for(let i =0;i<numbers.length;i++){
        sum=sum+numbers[i];
        product*=numbers[i];
    }
    const sumelement = document.getElementById("sum");
    const productelement = document.getElementById("product");
    sumelement.innerText = `your sum is ${sum}`;
    productelement.innerText = `your product is ${product}`;
}




// 16. Write a JavaScript program to implement a stack with push and pop operations. Find the top element of the stack and check if it is empty or not.
let stack=[];
function pushData(){
    let pushInputelement= document.getElementById("pushInput");
    let pushInput = pushInputelement.value;
    stack.push(pushInput);
    pushInputelement.value="";
}
function peekData(){
    let peekResult =document.getElementById("peekResult");
    peekResult.innerText=isEmpty()?"Stack is empty" :stack[stack.length - 1] ;;
}
function popData(){
    if(!isEmpty()){
        stack.pop();
        console.log("top element popped out")
    }
    else
    console.log("stack is empty")
}
function isEmpty(){
    if(stack.length>0){
        return false;
    }
    else 
    return true;
}
function displayIsEmptyorNot(){
    let isEmptyResult = document.getElementById("isEmptyResult");
    isEmptyResult.innerText=isEmpty()?"Your stack is empty":"Not Empty"
}



// 17. Write a JavaScript program that accepts a string as input and swaps the case of each character. For example if you input 'The Quick Brown Fox' the output should be 'tHE qUICK bROWN fOX'.
function question17(){
    let inputString = document.getElementById("17").value;
    let stringAfterSwap = document.getElementById("stringAfterSwap");
    var convertedString = '';

  for (let i = 0; i < inputString.length; i++) {
    let char = inputString.charAt(i);
    if (char === char.toLowerCase()) {
        convertedString += char.toUpperCase();
    } else {
        convertedString += char.toLowerCase();
    }
  }
  stringAfterSwap.innerText=convertedString;
}

// We have the following arrays :
// color = ["Blue ", "Green", "Red", "Orange", "Violet", "Indigo", "Yellow "];
// o = ["th","st","nd","rd"]
// Write a JavaScript program to display the colors in the following way :
// "1st choice is Blue ."
// "2nd choice is Green."
// "3rd choice is Red."
function displayColors(){
    let color = ["Blue", "Green", "Red", "Orange", "Violet", "Indigo", "Yellow "];
    let o = ["th","st","nd","rd"];
    for(let i=0;i<7;i++){
        let position=i+1;
        if(position>=1 && position<=3){
            console.log(`${position}${o[position]} choice is ${color[i]}`);
        }else{
            console.log(`${position}${o[0]} choice is ${color[i]}`);
        }
    }
}
 displayColors();




// 18.Write a JavaScript function to remove. 'null', '0', '""', 'false', 'undefined' and 'NaN' values from an array.
// Sample array : [NaN, 0, 15, false, -22, '',undefined, 47, null]
// Expected result : [15, -22, 47]
function question18(){
    let numberarray = [NaN, 0, 15, false, -22, '',undefined, 47, null];
    let newArray = [];
    for(let i=0;i<numberarray.length;i++){
        if(numberarray[i] !== '' && !isNaN(numberarray[i]) && numberarray[i] !== 0 && numberarray[i] !== null && numberarray[i] !== false && numberarray[i] !==  undefined){
            newArray.push(numberarray[i]);
        }
    }
    // let newArray = numberarray.filter(function (value) {
    //     return value !== null && value !== 0 && value !== '' && value !== false && value !== undefined && !isNaN(value);
    //   });
    console.log(newArray);
}
 question18();
